package attendants;

import javax.persistence.EntityManager;

import cars.Car;
import environment.Environment;
import environment.spatial.Map;
import environment.spatial.Point;
import environment.spatial.Zone;
import environment.time.Day;

import java.util.ArrayList;


public class ParkingAttController{
    private int numOfPA;
    private final ParkingAtt[] attendants;
    private Environment environment;
    private ArrayList<Car> cars;

    public ParkingAttController(int numOfPA, Environment environment, Car[] cars){
        System.out.println("ParkingAttController created.");
        this.numOfPA = numOfPA;
        this.attendants = new ParkingAtt[this.numOfPA];
        this.environment = environment;
        this.cars = new ArrayList<Car>();

        for (int i = 0; i < cars.length; i++)
            if(cars[i]!=null) {
                this.cars.add(cars[i]);
                System.out.println("Car " + i + " added to list.");
            }
        spawnAttendants();
    }

    private void spawnAttendants(){
        for(int i = 0; i < this.numOfPA; i++){
            this.attendants[i] = new ParkingAtt();
        }
    }

    public void proceed(){
        if(environment.getClock().getDay() != Day.SUNDAY && 9 < environment.getClock().getTime().getHour() && environment.getClock().getTime().getHour() < 21) {
            for (int i = 0; i < this.numOfPA; i++) {

                if (this.attendants[i].getCurrentCar() == null) {
                    this.attendants[i].proceed(cars.get(0));
                    cars.add(cars.get(0));
                    cars.remove(0);
                } else
                    this.attendants[i].processCar();
            }
        }
    }


    public int getNumberOfAttendants(){
        return this.numOfPA;
    }
}
