import attendants.ParkingAttController;
import cars.Car;
import cars.CarController;
import cars.strategy.DummyStrategy;
import cars.strategy.StrategyModule;
import environment.Environment;
import environment.spatial.Point;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.logging.*;
import picocli.CommandLine;
import picocli.CommandLine.*;


@Command(name = "carsharing", mixinStandardHelpOptions = true, version = "carsharing 0.1", description = "Runs a simulation of parking of rented cars.")
public class Main implements Runnable{

    // Set default values of simulation parameters here:

    @Option(names = {"-c", "--cars"}, description = "The number of cars in the simulated fleet.")
    private static int num_of_cars = 500;

    @Option(names = {"-p", "--parking-atts"}, description = "Number of parking attendants checking the parking zones and giving fines.")
    private static int num_of_parking_attendants = 1000;

    @Option(names = {"-d", "--days"}, description = "The lifetime of simulation in the number of days.")
    private static int simulation_days = 30;

    @Option(names = {"-u", "--customers"}, description = "The number of customers that use car sharing service each day.")
    private static int daily_customers = 2000;

    private static final Logger logger = Logger.getLogger( Main.class.getName() );



    // Simulation loop
    private static void simulation_loop(Environment env, CarController carController, ParkingAttController paController){
        logger.fine("Before for loop. env.getClock().isFinished() = " + env.getClock().isFinished());
        for( ; !env.getClock().isFinished() ; env.getClock().tick() ){
            //paController.parkingCheck();

            Point[] customers = env.spawnCustomers();
            if(customers != null) {
                //loop assigning customers to cars
                for (int i = 0; i < customers.length; i++) {
                    Car tempCar = carController.getNondrivingCar();
                    if (tempCar != null) {
                        tempCar.startDrivingTo(customers[i]);
                        env.customerServed();
                    } //TODO: else log that customer did not find the car I guess
                }
            }
            carController.proceed();
            paController.proceed();


        }
    }

    public void run() {
        long simulationLifetime = simulation_days * 24 * 60;
        
        //setting logging level for the program
        Logger rootLogger = LogManager.getLogManager().getLogger("");
        rootLogger.setLevel(Level.SEVERE); //fewer prints
        for (Handler h : rootLogger.getHandlers()) {
            h.setLevel(Level.SEVERE);
        }

        Environment env = new Environment(simulationLifetime, daily_customers); 
        StrategyModule strategy = new DummyStrategy();
        CarController carController = new CarController(num_of_cars, strategy, env);
        ParkingAttController paController = new ParkingAttController(num_of_parking_attendants, env, carController.getCars());

        logger.fine("Before main loop");
        simulation_loop(env, carController, paController);
        logger.fine("After main loop");

        System.out.println("Total money paid: " + carController.getTotalPayments());
        System.out.println("Number of fines: " + carController.getFineCount());
        System.out.println("Amount of money paid in fines: " + carController.getFineCount() * 150);
    }


    public static void main(String[] args){
        int exitCode = new CommandLine(new Main()).execute(args);
        System.exit(exitCode);
    }
}

