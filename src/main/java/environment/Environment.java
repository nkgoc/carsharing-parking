package environment;

import environment.spatial.Map;
import environment.spatial.Point;
import environment.time.Clock;
import java.util.logging.Logger;
import java.util.Random;


public class Environment{

    private int dailyCustomers;
    private Clock clock;
    private Map map;
    private int customersServed;
    private int customersPerMinute;
    private static final Logger logger = Logger.getLogger( Environment.class.getName() );
    private Random rand;


    public Environment(long simulationLifetime, int dailyCustomers){
        clock = new Clock(simulationLifetime);
        map = new Map();
        this.dailyCustomers = dailyCustomers;
        rand = new Random(System.currentTimeMillis());
        int tempPerMinute = (dailyCustomers - customersServed) / 1440; //calculating number of customers per minute
        //assuring there is at least one
        if(tempPerMinute == 0){
            customersPerMinute = 1;
        }else{
            customersPerMinute = tempPerMinute;
        }
        logger.info("Environment created.");
    }

    public Clock getClock(){
        return clock;
    }
    public Map getMap(){
        return map;
    }


    public Point[] spawnCustomers(){
        if(clock.isNewDayStarted()){
            customersServed = 0;
        }
        if(customersServed == dailyCustomers){
            return null;
        }

        int randomOffset = (rand.nextInt() % 2); //slight randomization (-1 deleted)

        Point[] customerArray = new Point[customersPerMinute + randomOffset];
        for(int i = 0; i<customersPerMinute + randomOffset; i++){
            customerArray[i] = map.getRandomParkable();
            customersServed++;
        }

        return customerArray;
    }

    public void customerServed(){
        customersServed++;
    }

}
