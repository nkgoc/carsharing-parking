package environment.time;

public class Clock{
    private long time;
    private long endTime;
    private int min;
    private int hour;
    private Day day;
    private boolean newDayFlag;

    public Clock(long endTime){
        day = Day.MONDAY;
        time = 0;
        this.endTime = endTime;
        newDayFlag = false;
    }
    /**
     * @return current clock's time encapsulated in a new object of Time
     */
    public Time getTime(){
        return new Time(this.day, this.hour, this.min);
    }

    public Day getDay(){
        return this.day;
    }

    public void tick(){
        newDayFlag = false;
        time++;
        min++;
        if (min % 60 == 0) {
          hour++;
          min = 0;
          if (hour >= 24){
           hour = 0;
           day = day.next();
           newDayFlag = true;
          }
        }

    }

    /**
     * Used to check if a new day has started.
     * @return true if current clock tick started a new day
     */
    public boolean isNewDayStarted(){
        return newDayFlag;
    }

    public boolean isFinished(){
        if(time == endTime)
            {return true;}
        else
            {return false;}
    }
}
