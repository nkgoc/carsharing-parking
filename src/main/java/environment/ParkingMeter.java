package environment;

import cars.tasks.Task;
import environment.spatial.Zone;
import environment.time.Day;
import environment.time.Time;

public class ParkingMeter {
    /**
     * The cost of a fine one gets for not paying for a parking within a zone.
     */
    public static double parkingFineCost = 150.0;
    /**
     * Minimal amount of Polish Złoty that can be paid to start parking payment.
     */
    public static double minParkingPayment = 2.0;
    /**
     * Starting at which hour (24 hour clock) it is necessary to pay for parking in
     * a zone.
     */
    public static int paidParkingHoursStart = 10;
    /**
     * After which hour is it no longer necessary to pay for parking (24 hour
     * clock).
     */
    public static int paidParkingHoursEnd = 20;
    /**
     * Used to limit the duration of the tasks so that it would be impossible to
     * create a task which spans over several non-free periods/days.
     */
    public static int maxTaskDuration = ((paidParkingHoursEnd - 24) + paidParkingHoursStart) * 60;


    // ---------------------------------------------------------------------------------------


    /**
     * Enforces payment rules on strategy modules. It does not calculate how much
     * did a parking cost, instead, it makes sure that strategy modules do not try
     * to break the simulation.
     * 
     * @param zone    - Zone for which the rules are checked.
     * @param time    - The time at which a parking task starts.
     * @param task    - The task to be passed to CarController.
     * @param payment - The maximum amount of money the StrategyModule expects to
     *                pay.
     * @return True if the task is found to be valid within the rules of the
     *         simulation.
     */
    public static boolean validate(Zone zone, Time time, Task task, double payment) {
        if (task.getDuration() > maxTaskDuration)
            return false;
        if (payment < minParkingPayment) { // make sure that minimal payment rule is obeyed
            return false;
        } else {
            double parking_cost = calculateParkingCostInZone(zone, time, task.getDuration());

            if (parking_cost > payment) {
                return false;
            }

            else {
                return true;
            }
        }
    }

    /**
     * After a parking is stopped (imagine a SkyCash parking payment stopping any
     * moment) the total payment is calculated by this function.
     * 
     * @param zone - The zone in which the car was parked.
     * @param startTime - The hour at which the task started executing.
     * @param taskDurationPassed - The actual time the task was being executed.
     * @param moneyOverflow - The status of extra, spare money stored in parking payment service app.
     * @return An array that under 0 index stores the amount of money that should be paid, and under
     * index 1 the new status of money overflow.
     */
    public static double[] calculateFinalPayment(Zone zone, Time startTime, int taskDurationPassed, double moneyOverflow) {
        double[] result = new double[2];

        result[1] = moneyOverflow;

        int minutesInPaidPeriod = durationInThePaidTime(startTime, taskDurationPassed);

        // The cases when minimal parking payment should be done:
        if (minutesInPaidPeriod <= zone.getMinutesForMinimalPayment()) {
            result[0] = minParkingPayment;
            if(moneyOverflow > minParkingPayment){
                // If we have enough overflow money to cover the payment
                // for parking, do it and decrease the overflow.
                result[0] = 0.0;
                result[1] -= minParkingPayment;
            } else {
                if(moneyOverflow > 0.0){
                    // If money overflow can cover payment only partially...
                    result[0] = minParkingPayment - moneyOverflow;
                    result[1] = 0.0;
                } else {
                    // We have no overflow money
                    result[0] = minParkingPayment;
                    result[1] = moneyOverflow; // should be equal to zero at this moment
                }
            }
            // An overflow should be generated if the time obtained for minimal payment
            // Is not utilized fully:
            result[1] += minParkingPayment - roundPayment(calculateParkingCostInZone(zone, startTime, taskDurationPassed));
        } else {
            double cost = roundPayment(calculateParkingCostInZone(zone, startTime, taskDurationPassed));
            if(moneyOverflow == 0.0){
                result[0] = cost;
                result[1] = moneyOverflow;
            }
            else {
                if(moneyOverflow >= cost){
                    result[0] = 0.0;
                    result[1] -= cost;
                }
                else{
                    result[0] = cost - moneyOverflow;
                    result[1] = 0.0; 
                }
            }
        }
        // TODO: Change those into decent logs

        // System.out.println(startTime + " + " + taskDurationPassed);
        // System.out.println("The money overflow: " + moneyOverflow + ", the payment: " + result[0] + ", minutes in paid period: "
        //        + minutesInPaidPeriod + ", the cost: " + roundPayment(calculateParkingCostInZone(zone, startTime, taskDurationPassed)) + 
        //        ", the final overflow status: " + result[1]);
        
        return result;
    }
    private static double roundPayment(double payment){
        double scale = Math.pow(10, 2); //we will use scale to move those decimal places before the comma/dot
        return Math.ceil(payment * scale) / scale; //having the payment in an integer format we round it up and go back two places after the comma
    }

    /**
     * A helper function
     */
    private static double calculateParkingCostInZone(Zone z, Time startTime, int duration) {
        double cost = z.getPrice() * (durationInThePaidTime(startTime, duration) / 60.0);
        return cost;
    }

    /**
     * A helper function
     */
    private static boolean isHourWithinPaidHours(Time t) {
        if (t.getHour() >= paidParkingHoursStart && t.getHour() < paidParkingHoursEnd) {
            return true;
        } else
            return false;
    }

    /**
     * A helper function
     */
    private static int durationInThePaidTime(Time startTime, int duration) {
        Time endTime = startTime.addTime(duration);

        if (isHourWithinPaidHours(startTime)) {
            if (isHourWithinPaidHours(endTime)) {
                // The whole duration of the task is within paid hours
                return duration;
            } else {
                // The task started in paid hours but it was finished when the parking
                // was already free.
                Time t = (new Time(Day.MONDAY, paidParkingHoursEnd, 0)).subtractTime(startTime.getHour(),
                        startTime.getMinutes());
                int h = t.getHour();
                int m = t.getMinutes();
                int minutesToPayFor = m + (60 * h);
                return minutesToPayFor;
            }
        } else {
            // If the task has not started within paid hours it could have finished
            // within those.
            if (isHourWithinPaidHours(endTime)) {
                Time t = (new Time(Day.MONDAY, endTime.getHour(), endTime.getMinutes()))
                        .subtractTime(paidParkingHoursStart, 0);
                int h = t.getHour();
                int m = t.getMinutes();
                int minutesToPayFor = m + (60 * h);
                return minutesToPayFor;
            } else {
                // If it neither started nor finished in paid time, it spanned whole
                // within free parking period.
                return 0;
            }
        }
    }

}
