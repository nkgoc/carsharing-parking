package environment;
import javax.persistence.*;

import environment.spatial.Point;
import environment.time.Time;
import java.util.logging.Level;
import java.util.logging.Logger;

@Entity
@Table(name="Events")
public class LogEvent {
    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private long id;
    @Column(name = "time", nullable = false)
    private Time time;
    @Embedded
    @Column(name="location", nullable = false)
    private Point location;
    @Column(name="entity_id", nullable = true)
    private long entity_id;
    @Transient
    private static final Logger logger = Logger.getLogger( LogEvent.class.getName() );

//    @Enumerated
//    @Column(name = "type", nullable = false)
    @Transient
    private EventType type;
    @Column(name = "type", nullable = false)
    private String eventTypeString;


    public LogEvent() {

    }

    public static enum EventType{
        CAR_STARTED(
            "CAR_STARTED",
            "car started moving",
            false),
        CAR_PARKED(
            "CAR_PARKED",
            "car parked",
            false),
        CAR_CHECKED(
            "CAR_CHECKED",
            "car checked by parking attendant",
            false),
        CAR_STARTED_PAYING(
            "CAR_STARTED_PAYING",
            "car started parking payment service",
            false),
        CAR_STOPPED_PAYING(
            "CAR_STOPPED_PAYING",
            "car finalized payment for parking",
            true),
        CAR_FINED(
            "CAR_FINED",
            "car got fined for an unpaind parking",
            true);

        @Column(name = "event_type", nullable = false)
        private String name;
        @Transient
        private String description;
        @Transient
        private boolean hasAssociatedValue;

        private EventType(String name, String description, boolean associated_value){
            this.name = name;
            this.description = description;
            this.hasAssociatedValue = associated_value;
        }
        public String getDescription(){
            return this.description;
        }
        public String toString(){
            return name;
        }
        public boolean hasAssociatedValue(){
            return hasAssociatedValue;
        }

    }
    @Column(name = "associated_value", nullable = true)
    private double value;

    public LogEvent(long id, Time time, Point point, EventType eventType){
        if(eventType.hasAssociatedValue() == true){
            logger.severe("Cannot use types CAR_PAID and CAR_FINED in a constructor which does not collect numeric value.");
            System.exit(1);
        }
        this.location = point;
        this.time = time;
        this.entity_id = id;
        this.type = eventType;
        this.eventTypeString = this.type.toString();


        logger.info(this.toString());
    }

    public LogEvent(long id, Time time, Point point, EventType eventType, double payment){
        this.location = point;
        this.time = time;
        this.entity_id = id;
        this.type = eventType;
        this.value = payment;
        this.eventTypeString = this.type.toString();

        logger.info("Log Event created: " + this);
    }
    public String toString(){
        String str;
        if(this.type.hasAssociatedValue()){
            str = "[" + this.time + "] Event: " + this.type + " regarding entity: " + this.entity_id + " with associated value: " + this.value + " occured at location: " + this.location;
        }
        else{
            str = "[" + this.time + "] Event: " + this.type + " regarding entity: " + this.entity_id + " occured at location: " + this.location;
        }
        return str;
    }

}
