package environment.spatial;

import java.lang.Math;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;

@Embeddable
public class Point{
    /**
     *  Point's x coordinate geographically
     */

    private double x;

    /**
     *  Point's y coordinate geographically
     */
    private double y;

    /**
     *  Default point constructor
     *
     */
    @Transient
    private Zone zone;

    @Column(name="zone", nullable = false)
    private String zoneString;

    public Point(double x, double y){
        this.x = x;
        this.y = y;
    }


    /**
     *  No argument point constructor setting x and y values to 0
     */
    public Point() {
        this.x = 0;
        this.y = 0;
    }

    /**
     *  Method getting x coordinate of a cell
     *
     *  @return double Cell's x-coordinate
     */
    public double getX(){
        return this.x;
    }

    /**
     *  Method setting new x value
     *
     */
    public void setX(double x){
        this.x = x;
    }

    /**
     *  Method getting y coordinate of a point
     *
     *  @return double Point's y-coordinate
     */
    public double getY(){
        return this.y;
    }

    /**
     *  Method setting new y value
     *
     */
    public void setY(double y){
        this.y = y;
    }

    public String toString(){
        return "'POINT(" + x + " " + y + ")'";
    }//this format is needed for database queries

    public void setZone(){
        if (Map.isWithinZone(this, Zone.A)) {this.zone = Zone.A;}
        else if (Map.isWithinZone(this, Zone.B)) {this.zone = Zone.B;}
        else if (Map.isWithinZone(this, Zone.C)) {this.zone = Zone.C;}
        else {this.zone = Zone.None;}
        this.zoneString = this.zone.toString();
    }

    public Zone getZone(){
        return this.zone;
    }

    public void setZone(Zone zone) {
        this.zone = zone;
    }

    public double getDistanceTo(Point destination) {
        //System.out.println("Distance to destination " + Math.hypot(this.x - destination.x,this.y - destination.y ) * 111000);
        return (Math.hypot(this.x - destination.x,this.y - destination.y ) * 111000); //multiplied by 111 000 in order to convert from spatial degrees to meters
    }

}