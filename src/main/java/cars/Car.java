package cars;
import javax.persistence.*;

import antlr.debug.Event;
import attendants.ParkingAtt;
import cars.strategy.StrategyModule;
import cars.tasks.*;
import environment.Environment;
import environment.spatial.Point;
import environment.spatial.Zone;
import environment.time.Time;
import environment.LogEvent;
import environment.ParkingMeter;
import java.util.logging.Logger;

@Entity
@Table(name = "Cars")
public class Car{


    @Id
    @Column(name="id", nullable = false)
    private long id;
    @Embedded
    @Column(name="location", nullable = true)
    private Point location;

    @Transient
    private Point destination;
    @Transient
    private CarController controller;

    @Transient
    private boolean isDriving = false;
    @Transient
    private int timeToDest;
    @Transient
    private boolean parkingPaid = false; //for the sake of simplicity let's assume that while the car waits for a client right after creation the parking is paid. Let's not, it ruins everything
    @Transient
    private boolean gotFine = false;
    @Transient
    private TaskQueue tasks;
    @Transient
    private Task currentTask = null;
    @Transient
    private double moneyOverflow;
    @Transient
    private static final Logger logger = Logger.getLogger( Environment.class.getName() );


    /**
     * A constant used as an estimate of how many minutes it takes
     * to travel one meter.
     */
    @Transient
    private final double distToTimeMultiplier = 0.0008; 
    /**
     * This constant controls capacity of every car's queue.
     */
    @Transient
    private final int queueCapacity = 32;

    public Car(long id, Point location, CarController controller){
        this.controller = controller;
        this.id = id;
        this.tasks = new TaskQueue(queueCapacity);
        this.moneyOverflow = 0.0;
        this.location = location;
        this.location.setZone();
        this.isDriving = false; // setting the driving flag to false will automatically force car to start performing parking-related tasks
        resetFine();
        logger.info("Created car at location " + location);
    }

    /**
     * public no-arg constructor for the sake of JPA standards
     */

    public Car(){
        controller = null;
        tasks = null;
    }

    /**
     * Perform what needs to be done for the current clock tick.
    */
    public void proceed(){
        if(this.isDriving) drive();
        else{
            if(this.currentTask == null){
                requestTasks(); // Remember to decrement the duration of the first task by one minute right after the request to maintain time integrity
            }
            else{
                performTasks();
            }
        }
    }

    /**
     * Interrupts the car making it drive with a new client.
     * The car is set into driving mode, its task queue gets cleared and current task forcefully finished.
     * @param destination 
     */
    public void startDrivingTo(Point destination){
        this.destination = destination;
        this.isDriving = true;
        this.timeToDest = calculateDrivingTime();
        
        stopParking();

        this.controller.getEnvironment().getMap().getEm().getTransaction().begin();
        this.controller.getEnvironment().getMap().getEm().persist(new LogEvent(this.id, this.controller.giveTime(), this.location, LogEvent.EventType.CAR_STARTED));
        this.controller.getEnvironment().getMap().getEm().getTransaction().commit();

    }

    /**
     * Will decrement a timer in the task and fetch next task from the queue when finished.
     * Requesting a new set of tasks when the number of tasks in queue drops to zero happens
     * in proceed() call.
     * @see Car#proceed()
     * @see Car#requestTasks()
     * @see TaskQueue#getTask()
     * @see Task#isFinished()
     */
    private void performTasks(){
        if(currentTask.isFinished()){ // Decrement task timer till done
            if(currentTask instanceof PaidParkingTask){
                payForParking(currentTask.getDuration());
            }
            currentTask = tasks.getTask(); // When done get yourself new task
            // If there are no more tasks available currentTask will be set to null
            // the null at currentTask field will cause the proceed() to ask for
            // a new set of tasks.
            if(this.currentTask == null) return; // <---- this one should be equivalent to "if(this.tasks.size() == 0)"
            if(this.currentTask instanceof WaitTask) startWaitTask();
            else startPaidParkingTask();
        }
    }
    private void startWaitTask(){
        this.parkingPaid = false;
    }
    private void startPaidParkingTask(){
        this.parkingPaid = true;
    }

    /**
     * Uses car's current location to specify how long it should be
     * driving to a given destination.
     * @return time to the destination
     * @apiNote Remember to call only after setting the location and destination fields of the class
     */
    private int calculateDrivingTime(){
        double distance = this.location.getDistanceTo(this.destination);
        return (int) java.lang.Math.ceil(distance * distToTimeMultiplier);
    }

    /**
     * Keeps car in the driving mode until the destination is reached.
     * @see Car#proceed()
     */
    private void drive(){
        if(this.timeToDest-- > 0){
            return;
        }
        park(this.destination);
    }

    /**
     * Sets isDriving to false which is equivalent to a 'parked' state.
     * @param location - after reaching the destination it should become
     * car's new location
     * @see Car#proceed()
     */
    private void park(Point location){
        this.location = location;
        this.location.setZone();
        this.isDriving = false; // setting the driving flag to false will automatically force car to start performing parking-related tasks
        resetFine();
        logger.info("A car has parked at " + location);

        this.controller.getEnvironment().getMap().getEm().getTransaction().begin();
        this.controller.getEnvironment().getMap().getEm().persist(new LogEvent(this.id, this.controller.giveTime(), this.location, LogEvent.EventType.CAR_PARKED));
        this.controller.getEnvironment().getMap().getEm().getTransaction().commit();

    }

    /**
     * Used when the parking process is interrupted. Runs a sequence of 
     * operations that inform other entities about the  change.
     * @see Car#startDrivingTo(Point)
     */
    private void stopParking(){
        if(currentTask instanceof PaidParkingTask){
            payForParking(currentTask.getTimeSpent());
        }
        this.currentTask = null;
        this.tasks.clearTasks();
    }

    private void payForParking(int duration){
        double[] result = ParkingMeter.calculateFinalPayment(
                            this.location.getZone(), 
                            this.controller.giveTime().subtractTime(duration), 
                            duration,
                            this.moneyOverflow
                            );
        double payment = result[1];
        this.moneyOverflow = result[0];
                        
        controller.payForParking(this.location, duration, payment);

        this.controller.getEnvironment().getMap().getEm().getTransaction().begin();
        this.controller.getEnvironment().getMap().getEm().persist(new LogEvent(this.id, this.controller.giveTime(), this.location, LogEvent.EventType.CAR_STOPPED_PAYING, payment));
        this.controller.getEnvironment().getMap().getEm().getTransaction().commit();

    }

    /**
     * Asks CarController to talk with StrategyModule to get new Tasks and starts them immediately
     * @see CarController#checkStrategyForCar(Point, boolean)
     * @see StrategyModule#createTasks(Point, boolean, Time)
     */
    private void requestTasks(){
       Task[] newTasks = controller.checkStrategyForCar(this.getLocation(), this.isFined());
       for(int i = 0; i < newTasks.length; i++){
           this.tasks.offer(newTasks[i]);
       }
       this.currentTask = this.tasks.getTask();
       performTasks(); // in the same tick the request was issued we should start performing the tasks
    }

    /**
     * Answers the question "Did the car get fined?"
     * @see Car#requestTasks()
     */
    public boolean isFined(){
        return this.gotFine;
    }

    /**
     * Give a fine to the car
     * @see ParkingAtt
     */
    public void fineForParking(){
        controller.payFine(this.location);
        this.gotFine = true;

        this.controller.getEnvironment().getMap().getEm().getTransaction().begin();
        this.controller.getEnvironment().getMap().getEm().persist(new LogEvent(this.id, this.controller.giveTime(), this.location, LogEvent.EventType.CAR_FINED, ParkingMeter.parkingFineCost));
        this.controller.getEnvironment().getMap().getEm().getTransaction().commit();


    }

    /**
     * Remove fine from car
     * @see CarController#resetFines()
     */
    public void resetFine(){
        this.gotFine = false;
    }

    public Point getLocation(){
        return location;
    }

    public void setLocation(Point location) {
        this.location = location;
    }

    /**
     * returns true if parking is validated by paying or by a previous parking ticket
     *
     */

    public boolean getParkingStatus(){
        if(this.gotFine || this.isDriving || this.location.getZone()==Zone.None){
            return true;
        }
        else {
            return parkingPaid;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Point getDestination() {
        return destination;
    }

    public void setDestination(Point destination) {
        this.destination = destination;
    }
    
    public boolean isDriving() {
        return isDriving;
    }
}
