package cars.strategy;

import cars.tasks.Task;
import cars.tasks.PaidParkingTask;
import environment.ParkingMeter;
import environment.spatial.Point;
import environment.time.Time;

/**
 * A very silly strategy that will get you fined all the time.
 */
public class DummyStrategy extends StrategyModule {

    protected TaskAndPayment[] createTasks(Point carLocation, boolean fineState, Time time){
        TaskAndPayment[] singleTask = new TaskAndPayment[1];
        Task actualTask = new PaidParkingTask(60);
        singleTask[0] = new TaskAndPayment(actualTask, 1000.0);
        return singleTask;
    }
    public void informStrategyOfPayment(Point carLocation, Time time, double payment){
        if(payment == ParkingMeter.parkingFineCost){
            // System.out.println("A car at location " + carLocation + " got fined!" + " (" + payment + ")");
        }
        else{
            // System.out.println("A car at location " + carLocation + " paid for parking " + payment + " PLN.");
        }
    }
    public DummyStrategy(){}
}

