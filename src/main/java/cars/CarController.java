package cars;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import attendants.ParkingAttController;
import cars.strategy.StrategyModule;
import cars.tasks.Task;
import environment.Environment;
import environment.LogEvent;
import environment.ParkingMeter;
import environment.spatial.Map;
import environment.spatial.Point;
import environment.spatial.Zone;
import java.util.logging.Logger;
import environment.time.Time;

import java.util.Random;
public class CarController{
    private Random rand = new Random(System.currentTimeMillis());
    private int numOfPA;
    private int numOfCars;
    private Car[] cars;
    private double totalPayments;
    private static final Logger logger = Logger.getLogger( CarController.class.getName() );

    public int getFineCount() {
        return fineCount;
    }

    private int fineCount;
    private StrategyModule strategy;
    private Environment environment;

    /**
     * The constructor requires a reference to the map to add cars
     * with initial positions. There should not be any way to add
     * Cars without initial position.
     * 
     * @param numOfCars - a number of all cars that are available in the fleet.
     * @param strategy of parking payments chosen for given simulation.
     */

    public CarController(int numOfCars, StrategyModule strategy, Environment environment){
        logger.info("CarController created.");
        this.numOfCars = numOfCars;
        this.cars = new Car[this.numOfCars];
        this.strategy = strategy;
        this.environment = environment;
        this.totalPayments = 0;
        this.fineCount = 0;
        spawnCars(this.environment.getMap());
    }

    private void spawnCars(Map map){
            Map.getEm().getTransaction().begin();
            for(int i = 0; i < this.numOfCars; i++){
                Point initialPos = map.getRandomParkable();
                this.cars[i] = new Car((long)i, initialPos, this);
                Map.getEm().persist(this.cars[i]);
            }
            Map.getEm().getTransaction().commit();
        }

    /**
     * Invoked on every clock tick
     */
    public void proceed(){
        for(int i = 0; i < numOfCars; i++){
            if(environment.getClock().isNewDayStarted()){ // check with clock if current tick starts a new day
                logger.info("A new day has started! It's " + environment.getClock().getDay() + ".");
                resetFines();
            }
            cars[i].proceed();
        }
    }


    /**
     * Talks with StrategyModule to establish new Tasks for a given.
     * The method supplements the Car's request with additional data
     * coming from the environment.
     * @param location
     * @param gotFined
     * @return
     */
    public Task[] checkStrategyForCar(Point location, boolean gotFined){
        Task[] tasks = strategy.giveTasks(
            location, 
            gotFined,
            environment.getClock().getTime() 
            );
        return tasks;
    }

    /**
     * Remove gotFine flag from all the cars
     * @see Car#resetFine()
     */
    public void resetFines(){
        for(int i = 0; i < numOfCars; i++){
            cars[i].resetFine();
    //TODO System.out.println("Fine on car " + cars[i].getId() + " has been reset");
        }
    }

    public void payFine(Point location) {
        this.fineCount++;
        updatePayments(location, ParkingMeter.parkingFineCost);
    }
    public double payForParking(Point location, int duration, double overflow){
        double[] payment = ParkingMeter.calculateFinalPayment(
            location.getZone(), 
            this.environment.getClock().getTime().subtractTime(duration), 
            duration,
            overflow
            );
        updatePayments(
            location, 
            payment[0]
            );
        return payment[1];
    }
    private void updatePayments(Point location, double payment) {
        this.totalPayments += payment;
        this.strategy.informStrategyOfPayment(location, environment.getClock().getTime(), payment);
    }
    public double getTotalPayments(){
        return totalPayments;
    }

    public void printSummary(){
        double moneySpentOnFines = ParkingMeter.parkingFineCost * fineCount;
        System.out.println("Current time: " + environment.getClock().getTime());
        System.out.println("Money spent on fines: " + moneySpentOnFines);
        System.out.println("Number of fines: " + fineCount);
        System.out.println("Money spent on tickets: " + (totalPayments - moneySpentOnFines));
    }

    public Car[] getCars() {
        return cars;
    }

    public Car getNondrivingCar() {
        int carIndex = rand.nextInt(cars.length);
        int limit = numOfCars;
        for (int i = 0; i<numOfCars; i++){
            if (!cars[carIndex].isDriving())
                return cars[carIndex];
        }
        return null;    // if car is not found null is returned
    }

    public Time giveTime(){
        return environment.getClock().getTime();
    }

    public Environment getEnvironment() {
        return environment;
    }

}
