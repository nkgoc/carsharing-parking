package cars.tasks;

import java.util.AbstractQueue;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

import cars.Car;

/**
 * A queue of tasks to be performed by a Car object.
 * @apiNote The task queue implementation is based on the following example:
 * https://www.demo2s.com/java/java-abstractqueue-extends.html
 * It is circular buffer queue. It has been however simplified 
 * in a way that makes it unsecure to perform concurrent access
 * to the queue. 
 * @see Task
 * @see Car
 * @see java.util.Queue
 */
public class TaskQueue extends AbstractQueue<Task> {
    private Task[] taskArray;
    private int head;
    private int tail;
    private int count;
    private int modcount;

    public TaskQueue(int capacity){
        this.taskArray = new Task[capacity];
        head = 0;
        tail = 0;
        count = 0;
    }

    /**
     * Adds a waiting task.
     * @param duration time to spend waiting
     * @return boolean telling if the task was added successfully
     * @see TaskQueue#(Task)
     */
    public boolean addWaitTask(int duration){
        return offer(new WaitTask(duration));
    }

    /**
     * Adds a task of paying and waiting a given time after that.
     * @param duration time untill next task
     * @param payment the amount of money to spend on a ticket
     * @return boolean telling if the task was added successfully
     * @see TaskQueue#offer(Task newTask)
     * @see TaskQueue#addWaitTask(int duration)
     */
    public boolean addPaidParkingTask(int duration){
        return offer(new PaidParkingTask(duration));
    }

    /**
     * A wrapper for poll method of Queue implementation.
     * @return the next task or a null if none is available
     * @see TaskQueue#poll()
     */
    public Task getTask(){
        return poll();
    }

    /**
     * Used to remove all the tasks from the queue.
     */
    public void clearTasks(){
        while(poll() != null);
    }

    /**
     * Part of Queue implementation. 
     * Used to add new tasks to the queue.
     * @param newTask
     * @return boolean telling if the object was added successfully
     */
    public boolean offer(Task newTask){
        assert newTask != null;
        if(count < taskArray.length){
            taskArray[tail] = newTask;
            tail = (tail+1) % taskArray.length;
            count++;
            modcount++;
            return true; 
        }
        else return false;
    }
    /**
     * Part of Queue implementation. Used to get a task and 
     * remove it from the queue.
     * @return the next task
     */
    public Task poll(){
        if(count == 0) return null;
        else{
            Task nextTask = peek();
            head = (head+1) % taskArray.length;
            count--;
            modcount++;
            return nextTask;
        }
    }
    /**
     * Part of Queue implementation. Allows to peek into
     * the next element in the without removing it.
     * @return the task that would go next
     * @see TaskQueue#poll()
     */
    public Task peek(){
        if(count == 0) return null;
        else return taskArray[head];
    }
    /**
     * Part of Container implementation.
     * @return the number of tasks in the queue
     */
    public int size(){
        return count;
    }
    /**
     * Part of Container implementation that allows 
     * iterating over the queue.
     */
    public Iterator<Task> iterator(){
        return new TaskQueueIterator();
    }


    /**
     * Implementation of iterator specific to the TaskQueue and
     * working as a nested class of TaskQueue.
     * @see TaskQueue
     */
    private class TaskQueueIterator implements Iterator<Task>{
        private int offset;
        private int modcountAtConstruction;

        public TaskQueueIterator(){
            this.offset = 0;
            this.modcountAtConstruction = modcount;
        }
        public Task next(){
            if(!hasNext()) throw new NoSuchElementException();
            Task i = taskArray[(head + offset) % taskArray.length];
            offset++;
            return i;
        }
        public boolean hasNext(){
            if(modcount != modcountAtConstruction) throw new ConcurrentModificationException();
            return (offset < count);
        }
        public void remove(){
            throw new UnsupportedOperationException();
        }
    }
    
}