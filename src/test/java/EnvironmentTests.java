import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import environment.Environment;

public class EnvironmentTests {

    @Test
    public void creatingClockInEnvironment(){
        
        Environment environment = new Environment(100, 5);

        assertNotEquals(null, environment.getClock());
    }

    @Test
    public void creatingMapInEnvironment(){
        Environment environment = new Environment(100, 5);
        assertNotEquals(null, environment.getMap());
    }

    @Test
    public void spawningCustomersTest(){
        Environment environment = new Environment(100, 5);
        assertNotEquals(null, environment.spawnCustomers());
    }

}
