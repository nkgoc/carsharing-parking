import static org.junit.Assert.assertEquals;
import org.junit.Test;
import environment.time.Day;

public class DayTests {
    @Test
    public void toStringRepresentation(){
        assertEquals("Monday", Day.MONDAY.toString());
        assertEquals("Tuesday", Day.TUESDAY.toString());
        Day d = Day.FRIDAY;
        assertEquals("It's Friday", "It's " + d);
    }
/*    @Test
    public void getNextDay(){
        Day d = Day.SATURDAY;
        d = d.next();
        assertEquals(Day.SUNDAY, d);
        d = d.next();
        assertEquals(Day.MONDAY, d);
    }*/
}
