import cars.tasks.PaidParkingTask;
import environment.spatial.Zone;
import environment.time.Day;
import environment.time.Time;
import org.junit.Test;
import environment.ParkingMeter;

import static org.junit.Assert.*;


public class ParkingMeterTests {

    @Test
    public void SampleTaskTest1() {

        Time time = new Time(Day.MONDAY,10,15);

        PaidParkingTask task = new PaidParkingTask(300);

        boolean check = ParkingMeter.validate(Zone.A, time, task, 30);

        assertTrue(check);

    }

    @Test
    public void SampleTaskTest2() {

        Time time = new Time(Day.FRIDAY,19,15);

        PaidParkingTask task = new PaidParkingTask(120);

        boolean check = ParkingMeter.validate(Zone.C, time, task, 8);

        assertTrue(check);

    }

    @Test
    public void NotAPaymentTimeTest() {

        Time time = new Time(Day.MONDAY, 1,0);

        PaidParkingTask task = new PaidParkingTask(300);

        boolean check = ParkingMeter.validate(Zone.A, time, task, 30);

        assertTrue(check);

    }

    @Test
    public void NoPaymentTest() {

        Time time = new Time(Day.MONDAY, 10,55);

        PaidParkingTask task = new PaidParkingTask(300);

        boolean check = ParkingMeter.validate(Zone.A, time, task, 0);

        assertFalse(check);

    }

    @Test
    public void NotEnoughPaymentTest() {

        Time time = new Time(Day.MONDAY, 10,55);

        PaidParkingTask task = new PaidParkingTask(300);

        boolean check = ParkingMeter.validate(Zone.A, time, task, 15);

        assertFalse(check);

    }

}
